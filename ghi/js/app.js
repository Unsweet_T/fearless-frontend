function formatDate(dateString) {
  const date = new Date(dateString);
  const chosen = { month: "2-digit", day: "2-digit", year: "numeric" };
  return date.toLocaleDateString(undefined, chosen);
}

function raiseAlert(message, type) {
  const alertWrapper = document.createElement("div");
  alertWrapper.classList.add("alert", `alert-${type}`, "mt-4");
  alertWrapper.setAttribute("role", "alert");
  alertWrapper.innerText = message;

  return alertWrapper;
}

function createCard(name, description, pictureUrl, startDate, endDate, locationName) {
  const reformattedStartDate = formatDate(startDate);
  const reformattedEndDate = formatDate(endDate);

  return `
    <div class="card" style="box-shadow: 0 0 10px rgba(0, 0, 0, 0.2); border-radius: 8px;">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 class="card-subtitle mb-2 text-muted">${locationName}</h6>
        <p class="card-text">${description}</p>
      </div>
      <div class="card-footer">
        <p class="card-text">${reformattedStartDate} - ${reformattedEndDate}</p>
      </div>
    </div>
  `;
}

window.addEventListener('DOMContentLoaded', async () => {
  const url = "http://localhost:8000/api/conferences/";

  try {
    const response = await fetch(url);

    if (!response.ok) {
      raiseAlert("Bad response", "warning");
    } else {
      const data = await response.json();
      let conferences = data.conferences;

      // Sort conferences by start date
      conferences.sort((a, b) => new Date(a.starts) - new Date(b.starts));

      const conferenceGrid = document.querySelector("div.conference-grid");

      // Clear conferenceGrid before appending the sorted conferences
      conferenceGrid.innerHTML = '';

      for (let i = 0; i < conferences.length; i++) {
        const conference = conferences[i];
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);

        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const title = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const startDate = details.conference.starts;
          const endDate = details.conference.ends;
          const locationName = details.conference.location.name;
          const html = createCard(title, description, pictureUrl, startDate, endDate, locationName);

          const conferenceContainer = document.createElement("div");
          conferenceContainer.classList.add("conference-container");
          conferenceContainer.innerHTML = html;

          conferenceGrid.appendChild(conferenceContainer);
        }
      }

      // Apply CSS Grid layout to conferenceGrid
      const numColumns = 3; // Adjust the number of columns as needed
      conferenceGrid.style.gridTemplateColumns = `repeat(${numColumns}, 1fr)`;
    }
  } catch (e) {
    const errorMessage = e.message || "An error occurred";
    raiseAlert(errorMessage, "Attention!");
    const container = document.querySelector(".container");
    container.insertBefore(alertWrapper, container.firstChild);
  }
});
